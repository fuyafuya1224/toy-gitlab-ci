from asyncio.log import logger
import json
import os
import urllib.request

def lambda_handler(event, context):
  channel_token = os.environ.get('LINE_CHANNEL_TOKEN')
  print(channel_token)

  for message_event in json.loads(event['body'])['events']:
    url = 'https://api.line.me/v2/bot/message/reply'
    headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + str(channel_token)
    }

    body = {
      'replyToken': message_event['replyToken'],
      'messages': [
        {
          "type": "text",
          "text": message_event['message']['text'],
        }
      ]
    }
    print(headers)

    req = urllib.request.Request(url, data=json.dumps(body).encode('utf-8'), method='POST', headers=headers)
    with urllib.request.urlopen(req) as res:
      logger.info(res.read().decode("utf-8"))

  return {
    'statusCode': 200,
    'body': json.dumps('Hello from Lambda!')
  }
