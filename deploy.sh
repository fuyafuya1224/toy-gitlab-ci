#!/bin/bash
set -euxo pipefail

create_docker_image() {
  AWS_ACCOUNT_ID=$1
  LAMBDA_DIR_NAME=$2

  IMAGE_REPO_NAME=lambda-$LAMBDA_DIR_NAME
  IMAGE_REPO_URI=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_REPO_NAME

  # 2. ECRリポジトリを作成（ディレクトリ名を用いる）
  if [ -z "$(aws ecr describe-repositories --repository-names $IMAGE_REPO_NAME)" ]; then
    aws ecr create-repository --repository-name $IMAGE_REPO_NAME
  fi

  # 3. Dockerfileからイメージをビルドし、「2」で作成したリポジトリにプッシュ
  cd ./app/$LAMBDA_DIR_NAME
  docker build --build-arg LINE_CHANNEL_TOKEN=$LINE_CHANNEL_TOKEN -t $IMAGE_REPO_NAME .
  docker tag $IMAGE_REPO_NAME:latest $IMAGE_REPO_URI:latest
  docker push $IMAGE_REPO_URI:latest

  # 4. ECRのイメージを利用してlambdaを最新のコンテナイメージで更新する
  aws lambda update-function-code --function-name $IMAGE_REPO_NAME --image-uri $IMAGE_REPO_URI:latest
}

AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query 'Account' --output text)

# 1. ECRにログイン
aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com

# ディレクトリ名を取得
LAMBDA_DIR_NAMES=`cd app && ls -l | awk '$1 ~ /d/ {print $9}'`
for LAMBDA_DIR_NAME in $LAMBDA_DIR_NAMES
do
  # 並列に実行する
  create_docker_image $AWS_ACCOUNT_ID $LAMBDA_DIR_NAME &
done

wait

echo All image created on `date`
